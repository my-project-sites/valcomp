<?php

# Имя базы данных для WordPress
define('DB_NAME', 'valcomp');

# Имя пользователя MySQL
define('DB_USER', 'root');

# Пароль к базе данных MySQL
define('DB_PASSWORD', '');

# Имя сервера MySQL
define('DB_HOST', 'localhost');

# Кодировка базы данных для создания таблиц.
define('DB_CHARSET', 'utf8');

# Схема сопоставления. Не меняйте, если не уверены.
define('DB_COLLATE', '');

/**#@+
 * Уникальные ключи и соли для аутентификации.
 *
 * Смените значение каждой константы на уникальную фразу.
 * Можно сгенерировать их с помощью {@link https://api.wordpress.org/secret-key/1.1/salt/ сервиса ключей на WordPress.org}
 * Можно изменить их, чтобы сделать существующие файлы cookies недействительными. Пользователям потребуется авторизоваться снова.
 *
 * @since 2.6.0
 */

define('AUTH_KEY',         '_[=$7|ND>98?p*.E~xx;={~M}yV79zeSIn>Z$2)9@YM<,V~-&NSp(Ut.%@R6v-im');
define('SECURE_AUTH_KEY',  ' K58iqq0M4bhy5(64-)k6hdy)8=;jq;383qz9,U<mD6=n6T6H4d/E]QD!]u`^(I>');
define('LOGGED_IN_KEY',    '|}M]t_j>Ldb1kUd7zi~||;=,#D.*` vdz*BXLByHi ,8!T~q@Pw_sTz%GL$SEZrv');
define('NONCE_KEY',        'L{VN%=mt9+sVcc|.H#F4X#OGL5U4Y+~%aBjFZxFkZoU`D3k}@$|+T)wYoI^uq,B ');
define('AUTH_SALT',        'z2|-ueo8u/j3M&3V#H5xZ7eP4.tL3gGc*fnAQb5`;zS `&w-5Dzqp[x_y,].?L~Z');
define('SECURE_AUTH_SALT', '|kh!Q`F/FBM{*.{>0/6(pIF$,v.7&E;D*>9!82I^<e=_i7p#4O^p`{^$a./:ImPN');
define('LOGGED_IN_SALT',   'Xsmqu@9&#]N>5IyLn3VK@~}U?e*j$zQ)L%`lhchg/<{G5$/L0@Qr% T7-P<P[Dja');
define('NONCE_SALT',       '<<fol6SyTPlfKgJt]99(`EV,tJDKOB7|4EPFiupzHkHL3]cQN1FZ /++`CyMGP5/');

/**#@-*/
/**
 * Префикс таблиц в базе данных WordPress.
 *
 * Можно установить несколько сайтов в одну базу данных, если использовать
 * разные префиксы. Пожалуйста, указывайте только цифры, буквы и знак подчеркивания.
 */

$table_prefix  = 'wp_';

/**
 * Для разработчиков: Режим отладки WordPress.
 *
 * Измените это значение на true, чтобы включить отображение уведомлений при разработке.
 * Разработчикам плагинов и тем настоятельно рекомендуется использовать WP_DEBUG
 * в своём рабочем окружении.
 * 
 * Информацию о других отладочных константах можно найти в Кодексе.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */

define('WP_DEBUG', false);
# Это всё, дальше не редактируем. Успехов!
# Абсолютный путь к директории WordPress.

if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

# Инициализирует переменные WordPress и подключает файлы.
require_once(ABSPATH . 'wp-settings.php');