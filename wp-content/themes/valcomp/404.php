<?php 
	
	get_header();
	wp_reset_postdata(); 
?>

<div class="container">
	<div class="row">
		<div class="col-12">
			<div class="content">
				<h1>Ооопс...похоже вы потерялись!</h1>
				<h2>Такой страницы не существует. Пожалуйста, воспользуйтесь меню сайта для навигации.</h2>	
			</div>
		</div>
	</div>
</div>

<?php get_footer(); ?>