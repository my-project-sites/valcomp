<?php get_header(); ?>

<div class="container">
	<div class="row">
		<div class="col-md-12">
			<h2>Задайте любой интересующий вопрос, оставьте свои контактные данные и мы вам обязательно перезвоним:</h2>
		</div>
		
		<div class="col-md-6">
			<div class="form">
				<?php echo do_shortcode('[wpforms id="1283" title="false"]');?>
			</div>
		</div>

		<div class="col-md-6">
			<ul class="contact_list list-unstyled">

				<li>
					<i class="fas fa-map-marker-alt"></i> <?=do_shortcode('[userAddress]');?>
				</li>

				<li>
					<a target="_blank" href="mailto:<?=do_shortcode('[userEmail]');?>">
						<i class="fas fa-envelope"></i> <?=do_shortcode('[userEmail]');?>
					</a>
				</li>

				<li>
					<a href="tel:<?=do_shortcode('[userPhone]');?>">
						<i class="fas fa-phone-square"></i> <?=do_shortcode('[userPhone]');?>
					</a>
				</li>

				<li>
					<i class="fab fa-vk"></i>
					<a href="<?=do_shortcode('[userVK]');?>">Вконтакте</a>
				</li>

				<li>
					<i class="fab fa-facebook"></i>
					<a href="<?=do_shortcode('[userFacebook]');?>">Facebook</a>
				</li>
			</ul>	
		</div>

		<div class="col-md-12">
			<div class="map">
				<script charset="utf-8" async src="https://api-maps.yandex.ru/services/constructor/1.0/js/?um=constructor%3A075d5e8fde7e1fd99060e2a9f2bea4a7f89c87aa6afb8aaafa2f8f747f26cc75&amp;width=100%&amp;height=100%&amp;lang=ru_RU&amp;scroll=false"></script>
	    	</div>
		</div>

	</div>
</div>

<?php get_footer(); ?>