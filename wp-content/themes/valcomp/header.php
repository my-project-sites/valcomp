<!DOCTYPE html>
<html lang="ru" itemscope itemtype="http://schema.org/WebPage">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
	<title></title>
		
	<?php wp_head(); ?>

</head>
<body>	
	<nav id="header" class="navbar navbar-expand-lg">
		<a class="header_phone" href="tel:+74012336106"><i class="fas fa-phone-square"></i></a>

		<a href="<?php echo get_home_url();?>" class="navbar-brand brand">
			<img class="brand_image" src="<?php echo get_template_directory_uri();?>/assets/images/logo.png" alt="logo">
			<p class="brand_paragraph">Раздвижные системы</p>
			<p class="brand_paragraph">«VALCOMP»</p>
		</a>

		<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#menu" aria-controls="menu" aria-expanded="false" aria-label="Toggle navigation">
			<i class="fas fa-bars"></i>
		</button>
		
		<div class="collapse navbar-collapse js-menu" id="menu">

			<?php

			wp_nav_menu( array(
			    'theme_location'  => 'top',
				'menu'            => '', 
				'container'       => '', 
				'container_class' => '', 
				'container_id'    => '',
				'menu_class'      => 'navbar-nav w-100 md-auto', 
				'menu_id'         => '',
				'echo'            => true,
				'fallback_cb'     => 'wp_page_menu',
				'before'          => '',
				'after'           => '',
				'link_before'     => '',
				'link_after'      => '',
				'items_wrap'      => '<ul id="%1$s" class="%2$s">%3$s</ul>',
				'depth'           => 0,
				'walker'          => '',
			));

			?>
		
		</div>

		<div class="collapse navbar-collapse" id="phone">
			<ul class="navbar-nav w-100 d-flex justify-content-around align-items-center">
				<li class="phone_paragraph">
					<i class="fas fa-phone-square"></i> <?=do_shortcode('[userPhone]');?>
				</li>
			</ul>
		</div>
	</nav>