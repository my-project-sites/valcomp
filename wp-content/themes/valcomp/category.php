<?php 

get_header();
$category = get_queried_object();
$id = $category->term_id;
$name = $category->name;

?>

<div id="content">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<h1><?=$name;?></h1>
			</div>
		</div>
		<div class="row">
			<?php

				$posts = get_posts( array(
					'numberposts' => 5,
					'category'    => $id,
					'orderby'     => 'date',
					'order'       => 'DESC',
					'include'     => array(),
					'exclude'     => array(),
					'meta_key'    => '',
					'meta_value'  =>'',
					'post_type'   => 'video',
					'suppress_filters' => true,
				));

				foreach($posts as $post) {
					setup_postdata($post);
					?><div class="col-md-4"><?php
				    	the_post_thumbnail();
				    ?></div><?php
				}

				wp_reset_postdata();

			?>
		</div>
	</div>
</div>

<?php get_footer(); ?>