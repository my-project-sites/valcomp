<?php 
	
	get_header();
	wp_reset_postdata(); 
?>

	<div class="container-fluid p-0">
		<div id="carouselExampleIndicators" class="carousel slide d-none d-md-block" data-ride="carousel">
		  <ol class="carousel-indicators">
		    <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
		    <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
		    <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
		    <li data-target="#carouselExampleIndicators" data-slide-to="3"></li>
		    <li data-target="#carouselExampleIndicators" data-slide-to="4"></li>
		  </ol>

		  <div class="carousel-inner">

		  	<div class="carousel-item active">
		      <img class="img-fluid" src="<?php echo get_template_directory_uri();?>/assets/images/slide3.jpg" alt="качественный товар">
		      <div class="carousel-caption d-none d-md-block">
			    <h3>У нас только качественный товар!</h3>
			    <p>Наши раздвижные системы характеризуются удобством и безопасностью использования. При этом они соответсвуют самым высоким требованиям к прочности и устойчивости.</p>
			  </div>
		    </div>

		    <div class="carousel-item">
		      <img class="img-fluid" src="<?php echo get_template_directory_uri();?>/assets/images/slide4.jpg" alt="интерьеры">
		      <div class="carousel-caption d-none d-md-block">
			    <h3>Наши раздвижные системы в интерьерах.</h3>
			    <p>Добавьте в свой интерьер движение!</p>
			  </div>
		    </div>

		  	<div class="carousel-item">
		      <img class="img-fluid" src="<?php echo get_template_directory_uri();?>/assets/images/slide5.jpg" alt="окна">
		      <div class="carousel-caption d-none d-md-block">
			    <h3>Решения для окон.</h3>
			    <p>Мы предлагаем современную оконную фурнитуру, а так же приводы для раздвижных и складывающихся ставней.</p>
			  </div>
		    </div>

		    <div class="carousel-item">
		      <img class="img-fluid" src="<?php echo get_template_directory_uri();?>/assets/images/slide1.jpg" alt="системы для промышленности">
		      <div class="carousel-caption d-none d-md-block">
			    <h3>Решения для промышленности. Cистемы подвесной транспортировки.</h3>
			    <p>Увеличьте производительность, экономьте складскую площадь и при этом снижайте затраты на техническое обслуживание.</p>
			  </div>
		    </div>

		    <div class="carousel-item">
		      <img class="img-fluid" src="<?php echo get_template_directory_uri();?>/assets/images/slide2.jpg" alt="шкафы-купе">
		      <div class="carousel-caption d-none d-md-block">
			    <h3>Решения для шкафов-купе.</h3>
			    <p>Благодаря нам, ваш интерьер приобретет новый характер. 25 лет мы предлагаем системы для раздвижных и складывающихся дверей для самостоятельного монтажа.</p>
			  </div>
		    </div>
		   
		  </div>

		  <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
		    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
		    <span class="sr-only">Previous</span>
		  </a>
		  <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
		    <span class="carousel-control-next-icon" aria-hidden="true"></span>
		    <span class="sr-only">Next</span>
		  </a>
		</div>
	</div>

	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="slogan">
					<?php the_content();?>
				</div>
			</div>
		</div>
	</div>

	<div class="container-fluid">
		<div class="row">
			<div class="col-md-12">
				<div class="slogan">
					<h2>НАШИ ИНТЕРЬЕРЫ</h2>
				</div>
			</div>
		</div>
	</div>

	<div class="container-fluid">
		<!-- Первый ряд -->
		<div class="row">
			<div class="col-md-3">
				<div class="grid">
					<figure class="effect-oscar">
						<img src="<?php echo get_template_directory_uri();?>/assets/images/main_1.jpg" alt="ATENA">
						<figcaption>
							<p><span>ATENA</span></p>
							<p>Для раздвижных межкомнатных дверей, с системой плавной доводки SILENT STOP</p>
							<a href="<?php echo do_shortcode('[main_url]');?>/doors/atena/">View more</a>
						</figcaption>			
					</figure>
				</div>
			</div>

			<div class="col-md-3">
				<div class="grid">
					<figure class="effect-oscar">
						<img src="<?php echo get_template_directory_uri();?>/assets/images/main_2.jpg" alt="JUPITER">
						<figcaption>
							<p><span>JUPITER</span></p>
							<p>Система для легких межкомнатных раздвижных дверей</p>
							<a href="<?php echo do_shortcode('[main_url]');?>/doors/jupiter/">View more</a>
						</figcaption>			
					</figure>
				</div>
			</div>

			<div class="col-md-3">
				<div class="grid">
					<figure class="effect-oscar">
						<img src="<?php echo get_template_directory_uri();?>/assets/images/main_3.jpg" alt="ARES-3">
						<figcaption>
							<p><span>ARES-3</span></p>
							<p>Система для раздвижных дверей шкафов и низких застроек интерьеров</p>
							<a href="<?php echo do_shortcode('[main_url]');?>/cupboards/ares-3/">View more</a>
						</figcaption>			
					</figure>
				</div>
			</div>

			<div class="col-md-3">
				<div class="grid">
					<figure class="effect-oscar">
						<img src="<?php echo get_template_directory_uri();?>/assets/images/main_4.jpg" alt="APOLLO">
						<figcaption>
							<p><span>APOLLO</span></p>
							<p>Система для складных межкомнатных дверей, дверей шкафов, ниш и дверных перегородок</p>
							<a href="<?php echo do_shortcode('[main_url]');?>/cupboards/apollo/">View more</a>
						</figcaption>			
					</figure>
				</div>
			</div>
		</div>
		<!-- Второй ряд -->
		<div class="row">
			<div class="col-md-3">
				<div class="grid">
					<figure class="effect-oscar">
						<img src="<?php echo get_template_directory_uri();?>/assets/images/main_5.jpg" alt="SATURN">	
						<figcaption>
							<p><span>SATURN</span></p>
							<p>Система для межкомнатных раздвижных дверей</p>
							<a href="<?php echo do_shortcode('[main_url]');?>/doors/saturn/">View more</a>
						</figcaption>			
					</figure>
				</div>
			</div>

			<div class="col-md-3">
				<div class="grid">
					<figure class="effect-oscar">
						<img src="<?php echo get_template_directory_uri();?>/assets/images/main_6.jpg" alt="ARES-2">
						<figcaption>
							<p><span>ARES-2</span></p>
							<p>Система для раздвижных дверей шкафов</p>
							<a href="<?php echo do_shortcode('[main_url]');?>/cupboards/ares-2/">View more</a>
						</figcaption>			
					</figure>
				</div>
			</div>

			<div class="col-md-3">
				<div class="grid">
					<figure class="effect-oscar">
						<img src="<?php echo get_template_directory_uri();?>/assets/images/main_7.jpg" alt="HORUS">
						<figcaption>
							<p><span>HORUS</span></p>
							<p>Система для раздвижных дверей шкафов</p>
							<a href="<?php echo do_shortcode('[main_url]');?>/cupboards/horus/">View more</a>
						</figcaption>			
					</figure>
				</div>
			</div>

			<div class="col-md-3">
				<div class="grid">
					<figure class="effect-oscar">
						<img src="<?php echo get_template_directory_uri();?>/assets/images/main_8.jpg" alt="HERKULES-120">
						<figcaption>
							<p><span>HERKULES-120</span></p>
							<p>Универсальная система для межкомнатных раздвижных дверей</p>
							<a href="<?php echo do_shortcode('[main_url]');?>/doors/herkules-120/">View more</a>
						</figcaption>			
					</figure>
				</div>
			</div>
		</div>
	</div>

	<div class="container-fluid help">
		<div class="row">
			<div class="col-12">
				<div class="slogan">
					<h2>НУЖНА ПОМОЩЬ?</h2>
				</div>
			</div>

			<div class="col-md-4">
				<div class="advice">
					<h3 class="advice_head">Установка и технические советы</h3>
					<img src="<?php echo get_template_directory_uri();?>/assets/images/down1.jpg" alt="Установка и технические советы">
					<a class="advice_link" href="<?php echo get_home_url();?>/category/heading-for-dors/"><span data-hover="Узнать больше">Узнать больше</span></a>
				</div>
			</div>

			<div class="col-md-4">
				<div class="advice">
					<h3 class="advice_head">Подбор систем</h3>
					<img src="<?php echo get_template_directory_uri();?>/assets/images/down2.jpg" alt="Подбор систем">
					<a class="advice_link" href="<?php echo get_home_url();?>/picking/"><span data-hover="Узнать больше">Узнать больше</span></a>
				</div>
			</div>

			<div class="col-md-4">
				<div class="advice">
					<h3 class="advice_head">Полезная информация</h3>
					<img src="<?php echo get_template_directory_uri();?>/assets/images/down3.jpg" alt="Полезная информация">
					<a class="advice_link" href="<?php echo get_home_url();?>/contacts/"><span data-hover="Узнать больше">Узнать больше</span></a>
				</div>
			</div>
		</div>
	</div>

<?php get_footer(); ?>