<?php 
	
	get_header();
	wp_reset_postdata(); 
?>

<div class="container">
	<div class="row">
		<div class="col-md-12">
			<div id="content">
				<h1><?php the_title();?></h1>
				<?php the_content();?>
			</div>	
		</div>
	</div>
</div>

<?php get_footer(); ?>