<?php get_header(); ?>

<div class="container">
	<div class="row">
		<div class="col-md-12">
			<div class="content">
				<h1>Мы и наши системы</h1>
				
				<p>Наша фирма работает на калининградском рынке с 1996 года предлагая широкий ассортимент качественных надёжных систем для раздвижных и складывающихся межкомнатных дверей и перегородок производства польской фирмы VALCOMP, а так же большой выбор систем для ставней , откатных и складывающихся промышленных дверей , ворот и подвесной транспортировки производства французской группы MONTION.</p><br>

				<p>Уникальность предлагаемых систем для домашнего использования характеризуется возможностью самостоятельной установки без привлечения профессионалов и использования сложного инструмента. Для каждой системы разработано подробное , но в то же время простое и понятное техническое описание по составу комплекта и монтажу. Есть оригинальные видео инструкции по установке.</p><br>

				<p>Предлагаемые системы отвечают самым жестким требованиям к фурнитуре для раздвижных и складывающихся дверей. Изделия протестированы производителем.</p><br>

				<p>Производитель предоставляет 25-летнюю гарантию на безаварийную работу систем для дверей и гарантирует , что они не имеют технических и производственных недостатков.</p><br>

				<p>В рамках предоставленной гарантии могут быть бесплатно заменены все дефектные элементы, входящие в комплект, при условии установки системы в строгом соответствии с монтажной инструкцией и эксплуатации изделия по назначению.</p><br>

				<p>Предлагаемые системы, благодаря своему новаторству и высокой надёжности заслужили уважение многих наших клиентов.</p>
			</div>
		</div>
	</div>

	<div class="row">
		<div class="col-md-12">
			<h2>Сертификаты</h2>
		</div>
	</div>

	<div class="row">
		<div class="col-md-4">
			<div class="certificate">
				<img class="certificate_img" src="<?php echo get_template_directory_uri();?>/assets/images/cert1.jpg" alt="img">
				<p class="certificate_text">Качество  было подтверждено в 2003 году международным сертификатом качества ISO 9001:2000.</p>
			</div>
		</div>

		<div class="col-md-4">
			<div class="certificate">
				<div class="certificate_img">
					<img src="<?php echo get_template_directory_uri();?>/assets/images/cert2.jpg" alt="img">
				</div>
	
				<p class="certificate_text">«VALCOMP» предлагает только качественные товары.</p>
			</div>
		</div>

		<div class="col-md-4">
			<div class="certificate">
				<img class="certificate_img" src="<?php echo get_template_directory_uri();?>/assets/images/cert3.png" alt="img">
				<p class="certificate_text">в соответствии с евро нормой EN 1527:1998 товары проверены на безаварийную работу в течении полных 100000 циклов.В том числе на стираемость и коррозийную стойкость.</p>
			</div>
		</div>
	</div>
</div> <!-- End container -->

<?php get_footer(); ?>