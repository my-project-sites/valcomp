	<footer>
		<a id="top" href="#" class="button">Вверх</a>

		<div class="container">
			<div class="row">
				<div class="col-md-4">
					<ul class="social">
						<li class="social_paragraph">
							<a class="social_link" href="<?=do_shortcode('[userVK]');?>">Вконтакте</a>
						</li>

						<li class="social_paragraph">
							<a class="social_link" href="<?=do_shortcode('[userFacebook]');?>">Facebook</a>
						</li>

						<li class="social_paragraph">
							<a class="social_link" href="tel:+74012336106"><?=do_shortcode('[userPhone]');?></a>
						</li>
					</ul>	
				</div>

				<div class="col-md-4">
					<div class="d-flex flex-column justify-content-center align-items-center">
						<a href="<?php echo get_home_url();?>" class="navbar-brand brand">
							<img class="brand_image" src="<?php echo get_template_directory_uri();?>/assets/images/logo.png" alt="logo">
							<p class="brand_paragraph">Раздвижные системы</p>
							<p class="brand_paragraph">«VALCOMP»</p>
						</a>
					</div>
				</div>

				<div class="col-md-4">
					<div class="float-md-right">
						<ul class="footermenu">
							<li>
								<a class="footermenu_link" href="<?php echo get_home_url();?>/private-policy/">Политика приватности</a>
							</li>
						</ul>
					</div>	
				</div>

				<div class="col-12">
					<div class="developers">
						<p class="developers_top-text">Develop by</p>
						<div class="developers_top">
							<a target="_blank" href="https://www.facebook.com/bloodborne.gothic/">
								<img class="at-logo" src="<?php echo get_template_directory_uri();?>/assets/images/my-logo.png" alt="my-logo">
							</a>
						</div>
						<p class="developers_bottom">All rights reserved &amp; copy</p>
					</div>
				</div>
			</div>
		</div>
	</footer>

	<?php wp_footer(); ?>

</body>
</html>