<?php get_header(); ?>

<div class="container-fluid">

	<div class="row">
		<div class="col-md-12">
			<h2>Чтобы выбрать соответствующую систему, необходимо определить:</h2>
		</div>
	</div>

	<div class="row">
		<div class="col-md-12">
			<div class="preview_block">
				<img src="<?php echo get_template_directory_uri();?>/assets/images/picking1.png" alt="preview">
				<img src="<?php echo get_template_directory_uri();?>/assets/images/picking2.png" alt="preview">
				<img src="<?php echo get_template_directory_uri();?>/assets/images/picking3.png" alt="preview">
				<img src="<?php echo get_template_directory_uri();?>/assets/images/picking4.png" alt="preview">
				<img src="<?php echo get_template_directory_uri();?>/assets/images/picking5.png" alt="preview">
			</div>
		</div>
	</div>

	<div class="row">
		<div class="col-md-12">
			<h3>В таблице приведены все доступные системы для заданных категорий.</h3>
		</div>
	</div>
</div>
<div class="container">
	<div class="row">
		<div class="col-md-12">
			<div class="tabs">
				<ul class="tabs__caption d-flex justify-content-center">
					<li class="active">Двери в шкафах</li>
					<li>Внутренние двери</li>
					<li>Промышленные ворота, тяжелые двери</li>
				</ul>

				<div class="tabs__content  active">
					<table class="table">
					  <thead class="thead-dark">
					    <tr>
					      <th scope="col">Вес двери</th>
					      <th scope="col">толщина полотна от 16 мм</th>
					      <th scope="col">толщина полотна от 18 мм</th>
					    </tr>
					  </thead>

					  <tbody>
					    <tr>
					      <td>9 кг</td>
					      <td class="tabble-row">
					      	<img src="<?php echo get_template_directory_uri();?>/assets/images/door-war.png" alt="img">
					      	<p><a href="#">Apis</a> мин. 16 мм</p>
					      </td>
					      <td></td>
					    </tr>

					    <tr>
					      <td>12 кг</td>
					      <td class="tabble-row">
					      	<img src="<?php echo get_template_directory_uri();?>/assets/images/door-war.png" alt="img">
					      	<p><a href="#">Tiwaz</a> мин. 16 мм</p>
					      </td>
					      <td></td>
					    </tr>

					    <tr>
					      <td>14 кг</td>
					      <td></td>
					      <td class="tabble-row">
					      	<img src="<?php echo get_template_directory_uri();?>/assets/images/door-green.png" alt="img">
					      	<p><a href="#">Apollo</a> 16-35 мм</p>
					      </td>
					    </tr>

					    <tr>
					      <td>25 кг</td>
					      <td class="tabble-row">
					      	<img src="<?php echo get_template_directory_uri();?>/assets/images/door-green.png" alt="img">
					      	<p><a href="#">Herkules Plus</a> 16-45 мм</p>
					      </td>
					      <td></td>
					    </tr>

					    <tr>
					      <td>40 кг</td>
					      <td class="tabble-row">
					      	<img src="<?php echo get_template_directory_uri();?>/assets/images/door-green.png" alt="img">
					      	<p><a href="#">Herkules Plus</a> 16-45 мм</p>
					      </td>
					      <td></td>
					    </tr>

					    <tr>
					      <td>45 кг</td>
					      <td class="tabble-row">
					      	<img src="<?php echo get_template_directory_uri();?>/assets/images/door-war.png" alt="img">
					      	<p><a href="#">Horus</a> 16-40 мм</p>
					      </td>
					      <td></td>
					    </tr>

					    <tr>
					      <td>50 кг</td>
					      <td></td>
					      <td class="tabble-row">
					      	<img src="<?php echo get_template_directory_uri();?>/assets/images/door-war.png" alt="img">
					      	<p><a href="#">Sara</a> 18 мм</p>
					      </td>
					    </tr>

					    <tr>
					      <td>70 кг</td>
					      <td class="tabble-row">
					      	<img src="<?php echo get_template_directory_uri();?>/assets/images/door-war.png" alt="img">
					      	<p><a href="#">Ares2</a> 16 мм</p>
					      </td>
					      <td></td>
					    </tr>

					    <tr>
					      <td>70 кг</td>
					      <td></td>
					      <td class="tabble-row">
					      	<img src="<?php echo get_template_directory_uri();?>/assets/images/door-war.png" alt="img">
					      	<p><a href="#">Ares3</a> 18 мм</p>
					      </td>
					    </tr>
					  </tbody>
					</table>
				</div>

				<!-- Вторая вкладка -->
				<div class="tabs__content">
					<table class="table">
					  <thead class="thead-dark">
					    <tr>
					      <th scope="col">Вес двери</th>
					      <th scope="col">толщина полотна от 16 мм</th>
					      <th scope="col">другая толщина двери</th>
					    </tr>
					  </thead>

					  <tbody>
					    <tr>
					      <td>14 кг</td>
					      <td></td>
					      <td class="tabble-row">
					      	<img src="<?php echo get_template_directory_uri();?>/assets/images/door-green.png" alt="img">
					      	<p><a href="#">Apollo</a> 18-35 мм</p>
					      </td>
					    </tr>

					    <tr>
					      <td>25 кг</td>
					      <td class="tabble-row">
					      	<img src="<?php echo get_template_directory_uri();?>/assets/images/door-green.png" alt="img">
					      	<p><a href="#">Hercules Plus</a> мин. 16-45 мм</p>
					      </td>
					      <td></td>
					    </tr>

					    <tr>
					      <td>30 кг</td>
					      <td class="tabble-row">
					      	<p><a href="#">Jupiter</a> мин. 16 мм</p>
					      </td>
					      <td></td>
					    </tr>

					    <tr>
					      <td>40 кг</td>
					      <td class="tabble-row">
					      	<img src="<?php echo get_template_directory_uri();?>/assets/images/door-green.png" alt="img">
					      	<p><a href="#">Herkules Plus</a> 16-45 мм</p>
					      </td>
					      <td></td>
					    </tr>

					    <tr>
					      <td>45 кг</td>
					      <td class="tabble-row">
					      	<p><a href="#">Saturn</a> мин. 16 мм</p>
					      </td>
					      <td></td>
					    </tr>

					    <tr>
					      <td>60 кг</td>
					      <td class="tabble-row">
					      	<p><a href="#">Herkules 60</a> мин. 16 мм</p>
					      </td>
					      <td></td>
					    </tr>

					    <tr>
					      <td>60 кг</td>
					      <td></td>
					      <td class="tabble-row">
					      	<p><a href="#">Hermes</a> 25-40 мм</p>
					      </td>
					    </tr>

					    <tr>
					      <td>60 кг</td>
					      <td></td>
					      <td class="tabble-row">
					      	<p><a href="#">Saf Power</a> 25-40 мм</p>
					      </td>
					    </tr>

					    <tr>
					      <td>80 кг</td>
					      <td></td>
					      <td class="tabble-row">
					      	<p><a href="#">Atena</a> мин. 22 мм</p>
					      </td>
					    </tr>

					    <tr>
					      <td>100 кг</td>
					      <td></td>
					      <td class="tabble-row">
					      	<img src="<?php echo get_template_directory_uri();?>/assets/images/door-glass.png" alt="img">
					      	<p><a href="#">Herkules Glass</a> 8-10-12 мм</p>
					      </td>
					    </tr>

					    <tr>
					      <td>120 кг</td>
					      <td></td>
					      <td class="tabble-row">
					      	<p><a href="#">Herkules 120</a> мин. 22 мм</p>
					      </td>
					    </tr>
					  </tbody>
					</table>
				</div>

				<div class="tabs__content">
					<table class="table">
					  <thead class="thead-dark">
					    <tr>
					      <th scope="col">Вес двери</th>
					      <th scope="col">Направляющие Sportub</th>
					      <th scope="col">Направляющие Hercule</th>
					    </tr>
					  </thead>

					  <tbody>
					    <tr>
					      <td>20 кг</td>
					      <td class="tabble-row">
					      	<img src="<?php echo get_template_directory_uri();?>/assets/images/door-fold.png" alt="img">
					      	<a href="#">Sportub 500</a>
					      </td>
					      <td></td>
					    </tr>

					    <tr>
					      <td>38 кг</td>
					      <td class="tabble-row">
					      	<img src="<?php echo get_template_directory_uri();?>/assets/images/door-fold.png" alt="img">
					      	<a href="#">Sportub 500</a>
					      </td>
					      <td></td>
					    </tr>

					    <tr>
					      <td>40 кг</td>
					      <td class="tabble-row">
					      	<img src="<?php echo get_template_directory_uri();?>/assets/images/door-fold.png" alt="img">
					      	<a href="#">Sportub 600</a>
					      </td>
					      <td></td>
					    </tr>

					    <tr>
					      <td>75 кг</td>
					      <td class="tabble-row">
					      	<img src="<?php echo get_template_directory_uri();?>/assets/images/door-fold.png" alt="img">
					      	<a href="#">Sportub 500/600</a>
					      </td>
					      <td></td>
					    </tr>

					    <tr>
					      <td>80 кг</td>
					      <td class="tabble-row">
					      	<a href="#">Sportub 3530</a>
					      </td>
					      <td></td>
					    </tr>

					    <tr>
					      <td>90 кг</td>
					      <td></td>
					      <td class="tabble-row">
					      	<a href="#">Hercule 9010</a>
					      </td>
					    </tr>

					    <tr>
					      <td>150 кг</td>
					      <td class="tabble-row">
					      	<img src="<?php echo get_template_directory_uri();?>/assets/images/door.png" alt="img">
					      	<a href="#">Sportub 500/600/5040</a>
					      </td>
					      <td></td>
					    </tr>

					    <tr>
					      <td>180 кг</td>
					      <td></td>
					      <td class="tabble-row">
					      	<a href="#">Hercule 9030</a>
					      </td>
					    </tr>

					    <tr>
					      <td>200 кг</td>
					      <td></td>
					      <td class="tabble-row">
					      	<img src="<?php echo get_template_directory_uri();?>/assets/images/door-arc.png" alt="img">
					      	<a href="#">Brama po luku</a>
					      </td>
					    </tr>

					    <tr>
					      <td>300 кг</td>
					      <td></td>
					      <td class="tabble-row">
					      	<img src="<?php echo get_template_directory_uri();?>/assets/images/door-fold.png" alt="img">
					      	<a href="#">Sportub 600</a>
					      </td>
					    </tr>

					    <tr>
					      <td>400 кг</td>
					      <td><a href="#">Sportub 6255</a></td>
					      <td><a href="#">Stainless steel</a> <br><br> <a href="#">Hercule 9040</a></td>
					    </tr>

					    <tr>
					      <td>900 кг</td>
					      <td></td>
					      <td><a href="#">Hercule 9040</a></td>
					    </tr>

					    <tr>
					      <td>2000 кг</td>
					      <td><a href="#">Sportub 8570</a></td>
					      <td><a href="#">Hercule 9060</a></td>
					    </tr>

					    <tr>
					      <td>3000 кг</td>
					      <td></td>
					      <td><a href="#">Hercule 9070</a></td>
					    </tr>

					  </tbody>
					</table>
				</div>	
			</div><!-- .tabs -->
		</div>
	</div>
</div> <!-- End container -->

<?php get_footer(); ?>

