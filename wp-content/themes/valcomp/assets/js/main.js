jQuery(document).ready(function($) {

    if(document.documentElement.clientWidth > 992) {
      $('#menu').removeClass('js-menu');
    }

    // slinky меню
    $('.js-menu').slinky({
      title: true
    });

    // Плавная прокрутка из подвала
    $('#top').click(function(e) {
        e.preventDefault();
        $([document.documentElement, document.body]).animate({
            scrollTop: $('#header').offset().top
        }, 1500);
    });

    // Уберем событие клика с «Системы для дверей»
    $('#menu-item-998 > a').click(function() {
      return false;
    });

    // Уберем событие клика с «Системы для шкафов»
    $('#menu-item-999 > a').click(function() {
      return false;
    });

    // Уберем событие клика с «Помощь»
    $('#menu-item-1020 > a').click(function() {
      return false;
    });

    // Уберем событие клика с «Видеотека»
    $('#menu-item-1022 > a').click(function() {
      return false;
    });

    // Уберем событие клика с «Наша продукция»
    $('#menu-item-1033 > a').click(function() {
      return false;
    });

    //Уберем событие клика со «Складных дверей»
    $('#menu-item-1024 > a').click(function() {
      return false;
    });

    // Уберем событие клика со «Ставней»
    $('#menu-item-1025 > a').click(function() {
      return false;
    });

    // Уберем событие клика с «Аксессуаров»
    $('#menu-item-1026 > a').click(function() {
      return false;
    });

    // Уберем событие клика с «Промышленных систем»
    $('#menu-item-1027 > a').click(function() {
      return false;
    });

    // Уберем событие клика с «Перила и балюстрады»
    $('#menu-item-1030 > a').click(function() {
      return false;
    });

    // Уберем событие клика с ЛОФТ СИСТЕМЫ
    $('#menu-item-1148 > a').click(function() {
      return false;
    });

    // Уберем событие клика с DESIGN SYSTEM
    $('#menu-item-1151 > a').click(function() {
      return false;
    });

    // Уберем событие клика с ROC DESIGN
    $('#menu-item-1154 > a').click(function() {
      return false;
    });

    // Вкладки где подбор систем
    function createCookie(name,value,days) {
        if (days) {
            var date = new Date();
            date.setTime(date.getTime()+(days*24*60*60*1000));
            var expires = "; expires="+date.toGMTString();
        }
        else var expires = "";
        document.cookie = name+"="+value+expires+"; path=/";
    }

    function readCookie(name) {
        var nameEQ = name + "=";
        var ca = document.cookie.split(';');
        for(var i=0;i < ca.length;i++) {
            var c = ca[i];
            while (c.charAt(0)==' ') c = c.substring(1,c.length);
            if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
        }
        return null;
    }

    function eraseCookie(name) {
        createCookie(name,"",-1);
    }

    $('ul.tabs__caption').each(function(i) {
        var cookie = readCookie('tabCookie' + i);
            if (cookie) {
                $(this).find('li').removeClass('active').eq(cookie).addClass('active')
                .closest('div.tabs').find('div.tabs__content').removeClass('active').eq(cookie).addClass('active');
        }
    });

    $('ul.tabs__caption').on('click', 'li:not(.active)', function() {
        $(this).addClass('active').siblings().removeClass('active')
        .closest('div.tabs').find('div.tabs__content').removeClass('active').eq($(this).index()).addClass('active');
        var ulIndex = $('ul.tabs__caption').index($(this).parents('ul.tabs__caption'));
        eraseCookie('tabCookie' + ulIndex);
        createCookie('tabCookie' + ulIndex, $(this).index(), 365);
    });

}); /*End ready*/