<?php
# Шорткод для home_url
function url_func() {
    return get_home_url();
}
add_shortcode( 'main_url', 'url_func' );

# Шорткод для site_name
function site_name() {
    $name = get_home_url();
    $number = stripos($name, '//');
    return 'www.' . substr($name, $number + 2);
}
add_shortcode( 'site_name', 'site_name' );

# Включим поддержку изображений записи
add_theme_support('post-thumbnails');

# Подключаем стили в шапке
add_action('wp_enqueue_scripts', 'header_styles');
function header_styles() {
	wp_enqueue_style('Fira+Sans', get_template_directory_uri() . '/assets/css/fonts.css');
	wp_enqueue_style('frontend', get_template_directory_uri() . '/assets/css/frontend.css');
	wp_enqueue_style('bootstrap', get_template_directory_uri() . '/assets/css/bootstrap.min.css');
	wp_enqueue_style('slinky', get_template_directory_uri() . '/assets/css/slinky.min.css', array(), false, '(max-width: 991px)');	
	wp_enqueue_style('main', get_template_directory_uri() . '/assets/css/main.css');
	wp_enqueue_style('mobile', get_template_directory_uri() . '/assets/css/mobile.css', array(), false, '(max-width: 991px)');
	wp_enqueue_style('correct', get_template_directory_uri() . '/assets/css/correct.css');
	wp_enqueue_style('style', get_stylesheet_uri());
}

# Подключаем скрипты в шапке
add_action('wp_enqueue_scripts', 'header_scripts');
function header_scripts() {
	
}

# Подключаем скрипты в футере
add_action('wp_footer', 'footer_scripts');
function footer_scripts() {
	wp_enqueue_script('fontawesome', get_template_directory_uri() . '/assets/js/fontawesome.js');
	wp_enqueue_script('bootstrap', get_template_directory_uri() . '/assets/js/bootstrap.min.js');
	wp_enqueue_script('slinkyjs', get_template_directory_uri() . '/assets/js/slinky.min.js');
	wp_enqueue_script('script', get_template_directory_uri() . '/assets/js/main.js');
}

# Регистрируем меню
register_nav_menus(array(
	'top'    => 'Главное меню',
));

# Удалить атрибут type у styles
add_filter('style_loader_tag', 'clean_style_tag');
function clean_style_tag($src) {
    return str_replace("type='text/css'", '', $src);
}

# Удалить атрибут type у scripts 
add_filter('script_loader_tag', 'clean_script_tag');
function clean_script_tag($src) {
    return str_replace("type='text/javascript'", '', $src);
}

# Удалим эмоции
remove_action('wp_head', 'print_emoji_detection_script', 7);
remove_action('wp_print_styles', 'print_emoji_styles');

# Асинхронная загрузка для скриптов, подключенных через wp_enqueue_script
add_filter('script_loader_tag', 'add_async_attribute', 10, 2);
function add_async_attribute($tag, $handle) {
	if(!is_admin()) {
	    if ('jquery-core' == $handle) {
	        return $tag;
	    }
	    return str_replace(' src', ' defer src', $tag);
	}
	else {
		return $tag;
	}
}

# Зарегистрируем новый тип записи
add_action( 'init', 'register_post_types' );
function register_post_types(){
	register_post_type('video', array(
		'label'  => null,
		'labels' => array(
			'name'               => 'Видео', # основное название для типа записи
			'singular_name'      => 'Видео', # название для одной записи этого типа
			'add_new'            => 'Добавить видео', # для добавления новой записи
			'add_new_item'       => 'Добавление видео', # заголовка у вновь создаваемой записи в админ-панели.
			'edit_item'          => 'Редактирование видео', # для редактирования типа записи
			'new_item'           => 'Новое видео', // текст новой записи
			'view_item'          => 'Смотреть видео', // для просмотра записи этого типа.
			'search_items'       => 'Искать видео', // для поиска по этим типам записи
			'not_found'          => 'Не найдено', // если в результате поиска ничего не было найдено
			'not_found_in_trash' => 'Не найдено в корзине', // если не было найдено в корзине
			'parent_item_colon'  => '', // для родителей (у древовидных типов)
			'menu_name'          => 'Видео', // название меню
		),
		'description'         => '',
		'public'              => true,
		'publicly_queryable'  => true, // зависит от public
		'exclude_from_search' => true, // зависит от public
		'show_ui'             => true, // зависит от public
		'show_in_menu'        => true, // показывать ли в меню адмнки
		'show_in_admin_bar'   => true, // по умолчанию значение show_in_menu
		'show_in_nav_menus'   => true, // зависит от public
		'show_in_rest'        => true, // добавить в REST API. C WP 4.7
		'rest_base'           => null, // $post_type. C WP 4.7
		'menu_position'       => 4,
		'menu_icon'           => 'dashicons-editor-video', 
		//'capability_type'   => 'post',
		//'capabilities'      => 'post', // массив дополнительных прав для этого типа записи
		//'map_meta_cap'      => null, // Ставим true чтобы включить дефолтный обработчик специальных прав
		'hierarchical'        => false,
		'supports'            => array('title'), // 'title','editor','author','thumbnail','excerpt','trackbacks','custom-fields','comments','revisions','page-attributes','post-formats'
		'taxonomies'          => array('category'),
		'has_archive'         => false,
		'rewrite'             => true,
		'query_var'           => true,
	) );
}

# Удалим метки
function unregister_taxonomy_post_tag() {
	register_taxonomy('post_tag', array());
}
add_action('init', 'unregister_taxonomy_post_tag'); 


# Отключаем Гутенберг
if('disable_gutenberg') {
	add_filter('use_block_editor_for_post_type', '__return_false', 100);
	remove_action('wp_enqueue_scripts', 'wp_common_block_scripts_and_styles');
	add_action('admin_init', function(){
		remove_action( 'admin_notices', ['WP_Privacy_Policy_Content', 'notice'] );
		add_action( 'edit_form_after_title', ['WP_Privacy_Policy_Content', 'notice']);
	});
}

# Подключение стилей в админке
add_action('admin_head', 'wph_inline_css_admin');
function wph_inline_css_admin() { echo 
	'<style>
		.post-type-video #postbox-container-1 {
			float: left !important;
			margin-right: 0 !important;
		}

		.plugin-title span a[href="http://www.never5.com/?utm_source=plugin&utm_medium=link&utm_campaign=what-the-file"],
		.post-type-video #edit-slug-box,
		.post-type-video #message,
		.wramvp_img_email_image,
		.upgrade_menu_link, 
		.proupgrade, .notice-error, .column-RV,
		.column-MV, .woocommerce-help-tip,
		.updated, #dashboard_php_nag,
		.mailpoet-dismissible-notice,
		.skiptranslate,
		.mailpoet_feature_announcement,
		.elementor-plugins-gopro,
		.aioseop-notice-woocommerce_detected,
		.aioseop_nopad_all,
		.aioseop_help_text_link, 
		.aioseop-metabox-pro-cta,
		aioseop-sitemap-prio-upsell,
		#aiosp_license_key_wrapper,
		#aioseop-notice-bar,
		.aioseop-sitemap-prio-upsell,
		.aioseop-notice-review_plugin_cta,
		.help_tip,
		.wbcr-upm-plugin-status,
		#wp-admin-bar-view-site,
		#wp-admin-bar-aioseop-pro-upgrade,
		.aioseo-upgrade-bar,
		#wp-admin-bar-aioseo-pro-upgrade,
		.aioseo-submenu-highlight,
		.wbcr-clearfy-factory-notice,
		.aioseo-score-button,
		.rank-math-column-display.no-score,
		.aioseo-review-plugin-cta,
		.rm-menu-new.update-plugins,
		.snippet-additional-keyphrases-row,
		.keyphrase-documentation,
		.setup-wizard-bg,
		.aioseo-menu-notification-counter,
		.aioseo-menu-notification-indicator,
		.row-actions .support
		{
			display: none !important;
		}
		#thumb {
			color: transparent;
		}
	</style>';
}

?>